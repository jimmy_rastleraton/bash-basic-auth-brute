#Bash Basic Auth Brute
simple slow and reliable http basic auth bruter

2 scriptes included:

brute.sh - Trys to brute a single ip:port given a users/passwords file

- ./brute.sh users.txt passes.txt 127.0.0.1:80 output.txt

scanlist.sh - Same as brute.sh except it will run through a whole list of ips:ports

- ./scanlist.sh list.txt users.txt passes.txt output.txt

For both scripts having the output_file is optional
